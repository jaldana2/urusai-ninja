<!DOCTYPE html>
<html>
<head>
	<title>urusai!</title>
	<style type="text/css">
		* { font-family: 'Lato', sans-serif; margin: 0; padding: 0; }
		a { text-decoration: none; }
		body { background-color: #EFEFEF; }
		
		@media screen and (min-width: 361px) {
			#header { padding: 4px 20px; }
		}
		@media screen and (max-width: 360px) {
			#header { padding: 4px 5px; }
		}
		#header { box-sizing: border-box; height: 48px;  margin: auto; background-color: #333; color: white; overflow: hidden; }
		#header h1 { font-size: 40px; display: inline-block; margin-right: -4px; }
		#header h2 { font-size: 18px; line-height: 40px; display: inline-block; margin-right: 20px; }
		#header a.button { color: black; font-weight: bold; display: inline-block; padding: 0 10px; position: relative; bottom: -2px; background-color: #EFEFEF; border-top-left-radius: 5px; border-top-right-radius: 5px; box-shadow: 0px 0px 2px black; }
		#header a.button:hover { background-color: #A4BEE0; background: linear-gradient(to bottom, #A4BEE0 0%, #EFEFEF 40%, #EFEFEF 100%); }
		#header a.home { color: white; }
		#header a.home:hover { color: #A4BEE0; transition: color 0.5s ease; }
		
		#content { max-width: 800px; margin: auto; word-wrap: break-word; margin-top: 20px; margin-bottom: 20px; }
		#content div.block { height: 28px; overflow: hidden; padding: 8px; }
		#content div.block a { color: #333; }
		#content div.block a:hover { color: #A4BEE0; }
		#content div.block h1 { font-size: 24px; display: inline-block; max-width: 70%; vertical-align: bottom; font-weight: normal; }
		#content div.block h2 { font-size: 16px; display: inline-block; vertical-align: bottom; font-weight: normal; }
		#content div.block h1 { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }
		#content h3.hbar { background-color: #333; background: linear-gradient(to right, #333 0%, #EFEFEF 100%);  color: white; padding: 2px 10px; }
		
		#content div#listing { text-align: center; }
		#content div#listing a { display: inline-block; width: 40%; box-sizing: border-box; text-align: center; font-size: 18px; background-color: #333; color: white; border-radius: 8px; padding: 8px; margin-top: 4px; }
		#content div#listing a:hover { color: #A4BEE0; }
		#content div#listing h4 { font-weight: normal; font-size: 18px; }
		
		#footer { margin-top: 100px; text-align: center; }
		#footer a { color: black; }
		#footer a:hover { color: #444; }
	</style>
	<link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	<link href='//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=360, initial-scale=1, maximum-scale=1, minimum-scale=1">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
</head>
<body>
	<div id='header'>
		<a href='/' class='home'>
			<h1>urusai!</h1>
			<h2>ninja&nbsp;&nbsp;&#12358;&#12427;&#12373;&#12356;</h2>
		</a>
		<a href='?page=home' class='button'>home</a>
		<a href='?page=ost' class='button'>ost</a>
	</div>
	<div id='content'>
		<?php
			include("engine.php");
			$page = "home";
			$epiDB = readDB("epi.db");
			knatsort($epiDB);
			knatsort($epiDB["__/status"]);
			if (isset($_GET["page"])) $page = $_GET["page"];
			
			switch($page) {
				case "stat":
					echo file_get_contents("http://spectrum.aftermirror.com/stat.file");
				break;
				case "ost":
					echo "<h4>soon...</h4>";
				break;
				case "view":
					echo "<h2>{$_GET['anime']}</h2><br/><br/>";
					echo "<div id='listing'>";
					$ep = $_GET["ep"];
					$data = $epiDB[$_GET["anime"]][$ep];
					//foreach ($epiDB[$_GET["anime"]] as $ep => $data) {
						/*echo "
							<h4>episode {$ep}</h4>
							<a href='//nyc-01.aftermirror.com/anime/high/{$data}.mp4'>720p+</a>
							<a href='//nyc-01.aftermirror.com/anime/low/{$data}.mp4'>360p</a>
						";*/
						echo "
							<h4>episode {$ep}</h4>
							<a href='//nyc-01.aftermirror.com/anime/high/{$data}.mp4' class='media-button'>720p+</a>
							<a href='//nyc-01.aftermirror.com/anime/low/{$data}.mp4' class='media-button'>360p</a>
							<br/>
							<br/>
							<video id='video' controls style='width: 100%;'></video>
							<br/>
							<a href='#' class='screen-shot'><span class='fa fa-camera'></span>&nbsp;&nbsp;take a screenshot</a>
							<script>
								var video = document.getElementById('video');
								var source = document.createElement('source');
								var time = 0;
								
								$(function() {
									video.appendChild(source);
									$('a.media-button').on('click', function(e) {
										if (video.currentTime > 0) {
											time = video.currentTime;
										}
										
										source.setAttribute('src', $(this).attr('href'));
										source.setAttribute('type', 'video/mp4');
										video.load();
										
										video.currentTime = time;
										
										e.preventDefault();
										return false;
									});
									$('a.screen-shot').on('click', function(e) {
										if (video.currentTime > 0) {
											video.pause();
											time = video.currentTime;
											window.open('//snap.urusai.ninja/{$data}/' + time, '_blank');
										}
									});
								});
							</script>
						";
					//}
					echo "</div>";
				break;
				default:
					$list = array();
					foreach ($epiDB["__/status"] as $anime => $data) {
						foreach ($data as $ep => $stat) {
							if ($stat != "downloading") {
								$sha = $epiDB[$anime][$ep];
								$tago = time() - $epiDB["__/status"][$anime][$ep];
								$list[$tago] = array(
									"anime" => $anime,
									"ep" => $ep,
									"tago" => $tago,
									"sha" => $sha
								);
							}
							else {
								$list[time()] = array(
									"anime" => $anime,
									"ep" => $ep,
									"tago" => false,
									"sha" => $sha
								);
							}
						}
					}
					knatsort($list);
					$tagos = array();
					foreach ($list as $data) {
						$ago = time_since($data["tago"]);
						if ($data["tago"] < 60 * 60 * 24) {
							// today
							$ago = "today";
						}
						elseif ($data["tago"] < 60 * 60 * 24 * 7) {
							$ago = "this week";
						}
						else {
							$ago .= " ago";
						}
						if (!isset($tagos[$ago])) {
							echo "<h3 class='hbar'>{$ago}</h3>";
							$tagos[$ago] = true;
						}
						$epstr = "episode <b>{$data['ep']}</b>";
						if ($data["ep"] == "Movie") {
							$epstr = "<b>{$data['ep']}</b>";
						}
						echo "
							<div class='block'>
								<a href='?page=view&anime={$data['anime']}&ep={$data['ep']}'>
									<h1>{$data['anime']}</h1>&nbsp;<h2>{$epstr}</h2>
								</a>
							</div>
						";
					}
				break;
			}
		?>
		<div id='footer'>
			<small><a href='/'>urusai!ninja</a> ~ <a href='//aftermirror.com'>after|<b>mirror</b></a> ~ <a href='https://bitbucket.org/jaldana2/urusai-ninja'>source (git@bitbucket)</a></small>
		</div>
	</div>
</body>
</html>
